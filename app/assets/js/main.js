const preloader = '<div class="preloader">' + '<div class="container">' + '<div class="logo">' + '<span class="sr-only">{{ appName }}</span>' + '<img src="' + siteBaseUrl + '/assets/images/logo.png" alt="">' + '</div>' + '</div>' + '</div>';

$(document).ready(function () {

    $('span.copyright-year').text(new Date().getFullYear());

    if ($('.p-home').length) {
        $('.img-slider--s1 .owl-carousel').owlCarousel({
            items: 1,
            autoplay: true,
            loop: true,
            dots: true,
            dotsContainer: $('.img-slider--s1').find('.img-slider__dots')
        });

        $('.img-slider--s2 .owl-carousel').owlCarousel({
            items: 1,
            autoplay: true,
            loop: true,
            dots: true,
            dotsContainer: $('.img-slider--s2').find('.img-slider__dots')
        });

        $('.article-carousel--s1 .owl-carousel').owlCarousel({
            items: 3,
            autoplay: true,
            margin: 20,
            loop: true,
            dotsContainer: $('.article-carousel--s1').find('.article-carousel__dots'),
            nav: true,
            navContainer: $('.article-carousel--s1').find('.article-carousel__nav'),
            navText: ['<img src="/assets/images/chevron-left.svg" alt="">', '<img src="/assets/images/chevron-right.svg" alt="">'],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        });

        $('.article-carousel--s2 .owl-carousel').owlCarousel({
            items: 3,
            autoplay: true,
            margin: 20,
            loop: true,
            dotsContainer: $('.article-carousel--s2').find('.article-carousel__dots'),
            nav: true,
            navContainer: $('.article-carousel--s2').find('.article-carousel__nav'),
            navText: ['<img src="/assets/images/chevron-left.svg" alt="">', '<img src="/assets/images/chevron-right.svg" alt="">'],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                992: {
                    items: 3
                }
            }
        });

        $(".merchandise-carousel").Cloud9Carousel({
            yOrigin: 42,
            yRadius: 48,
            // mirror: {
            //     gap: 12,
            //     height: 0.2
            // },
            frontItemClass: 'front-item',
            autoPlay: 1,
            bringToFront: true
        });
    }

    //    toolkit page
    $('.explore-item').each(function () {
        var toolkitWidth = $(this).width();
        $(this).height(toolkitWidth);

        $(window).resize(function () {
            $(this).height(toolkitWidth);
        });
    });



    $('.same-height').matchHeight({
        byRow: true,
    });



    $('.event-gallery').slick({
        dots: false,
        arrows: true,
        autoplay: true,
        infinite: true,
        speed: 300,
    });





    $(".inq-btn").on("click", function () {

        var $button = $(this);
        var oldValue = $button.closest('.sp-quantity').find("input.quntity-input").val();

        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        $button.closest('.sp-quantity').find("input.quntity-input").val(newVal);

    });


    //    scroll

    if ($('.fp-well').length) {
        new PanelSnap({
            container: document.body,
            panelSelector: '.fp-well section',
            directionThreshold: 10,
            delay: 20,
            duration: 1000,
            easing: function (t) { return t; }
        });
    }

    $('.animated').each(function (i, el) {
        if (inView.is(el)) {
            console.log(this);
            $(this).addClass('animate__animated').removeClass('animated');
        }
    });

    inView('.animated').once('enter', function () {
        $(this).addClass('animate__animated').removeClass('animated');
    });


});